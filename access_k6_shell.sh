#!/usr/bin/env bash
#Script created to access the k6 pod directly from the current terminal.

working_dir=`pwd`

#Get namesapce variable
tenant=`awk '{print $NF}' $working_dir/tenant_export`

#Get k6 pod details

k6_server_pod=`kubectl get po -n $tenant | grep k6-server | awk '{print $1}'`

#Exec into the k6 pod

kubectl exec -ti -n $tenant $k6_server_pod -- sh
