# k6-on-k8s

This is an attempt to create k6 load testing on a kubernetes cluster.

k6 is a modern load testing tool, building on Load Impact's years of experience in the load and performance testing industry. It provides a clean, approachable scripting API, local and cloud execution, flexible configuration, with command & control through CLI or a REST API.

Please see https://github.com/loadimpact/k6 or https://k6.io/ for more information and documentation.

**Pre-requisites:**

  1.) Existing kubernetes cluster, this was tested using version 1.11.2, this should work on earlier versions as well.
  
  2.) Linux/Unix system.

## Procedure:

**Step 1: Clone the repo -**

    git clone https://infinitydon@bitbucket.org/infinitydon/k6-on-k8s.git




**Step 2: Create a k6 deployment -**

    ./k6-cluster-create.sh  (This will ask you to create a namespace)

  Wait until all the PODs are in "Running" status  

      kubectl -n k6-loadtest get   

      NAME                          READY     STATUS    RESTARTS   AGE
      influxdb-k6-bcdbd7786-xxrh8   1/1       Running   0          1d
      k6-grafana-67dc898547-nsqpj   1/1       Running   0          1d
      k6-server-5fd6b87b69-4wnkg    1/1       Running   0          1d



**Step 3: Create the Influxdb datasource inside Grafana -**

    ./dashboard.sh


**Step 4: Import the Grafana dashboard -**

  **A.** Access the Grafana, this depends on whether you used a nodeport or loadbalancer, for this example a Loadbalancer was used
  If you are not a public cloud or don't have a Load Balancer deployment, then use the NodePort (this is the default configuration).
  Sample given below:

       kubectl -n k6-loadtest get service -o wide

       NAME          TYPE           CLUSTER-IP       EXTERNAL-IP                             PORT(S)             AGE       SELECTOR
       influxdb-k6   ClusterIP      100.67.9.140     <none>                                  8083/TCP,8086/TCP   1d        app=influxdb-k6
       k6-grafana    LoadBalancer   100.65.202.218   xxxxxx.eu-central-1.elb.amazonaws.com   3000:30903/TCP      1d        app=k6-grafana

  Using the above as an example, I will need to open http://xxxxxx.eu-central-1.elb.amazonaws.com:3000/ in order to be able to access the Grafana UI.

  **B.** Upload the dashboard JSON file (k6-load-testing-results_rev3.json) and select the InfluxDB Datasource name which is "k6db"



**Step 5: Access the k6 deployment pod so as to be able to run the load test.**

    ./access_k6_shell.sh



**Step 6: Start a load test -**

     k6 run --out influxdb=http://influxdb-k6:8086/myk6db --vus 500 --duration 40m script.js
 
  You should be able to see the test results in your grafana, you can use the test_script.js to test



**Step 7: Exit from the shell -**

    Simply types "exit" and the enter key



**Step 8: Uninstall -**

  Simply delete the namespace that was created:

        kubectl delete namespace k6-loadtest
