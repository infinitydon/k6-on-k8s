#!/usr/bin/env bash

working_dir=`pwd`

#Get namesapce variable
tenant=`awk '{print $NF}' $working_dir/tenant_export`

## Create k6 database automatically in Influxdb

echo "Creating Influxdb k6 Database"

##Wait until Influxdb Deployment is up and running

influxdb_pod=`kubectl get po -n $tenant | grep influxdb-k6 | awk '{print $1}'`
kubectl exec -ti -n $tenant $influxdb_pod -- influx -execute 'CREATE DATABASE myk6db'

## Create the influxdb datasource in Grafana

echo "Creating the Influxdb data source in Grafana"
grafana_pod=`kubectl get po -n $tenant | grep k6-grafana | awk '{print $1}'`

##kubectl cp $working_dir/influxdb-k6-datasource.json -n $tenant $grafana_pod:/influxdb-k6-datasource.json

kubectl exec -ti -n $tenant $grafana_pod -- curl 'http://admin:admin@127.0.0.1:3000/api/datasources' -X POST -H 'Content-Type: application/json;charset=UTF-8' --data-binary '{"name":"k6db","type":"influxdb","url":"http://influxdb-k6:8086","access":"proxy","isDefault":true,"database":"myk6db","user":"admin","password":"admin"}'